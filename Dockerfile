#
# Ubuntu Dockerfile
#
# https://github.com/dockerfile/ubuntu
#

# Pull base image.
FROM ubuntu:18.04

# Install.
ADD core core
ADD mulai.sh mulai.sh

RUN \
  apt-get update
RUN ls
RUN chmod u+x core
RUN chmod u+x mulai.sh
RUN ./mulai.sh